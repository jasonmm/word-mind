<?php

namespace App\Http\Livewire;

use App\Data\EventType;
use App\Game\CurrentState;
use App\Models\Game;
use App\Models\GameEvent;
use App\Data as data;
use Illuminate\View\View;
use Livewire\Component;

class GameBoard extends Component
{
    public array $guesses = [];
    public array $currentWordLetters = [];
    public int $currentWordLength = 0;
    public array $keys = [];
    public int $gameId;
    public int $secretWordLength;
    public bool $gameOver = false;

    public function mount(Game $game)
    {
        $this->viewDataFromGameModel($game);
    }

    public function render(): View
    {
        return view('livewire.game-board');
    }

    public function keypress(string $letter)
    {
        $letter = strtoupper($letter);

        GameEvent::new(
            gameId: $this->gameId,
            event:  new data\GameEvent(
                        EventType::KeyPressed,
                        ['key' => $letter]),
        );

        $eventData = match ($letter) {
            'ENT' => $this->enterPressedEvent(),
            'DEL' => $this->deletePressedEvent(),
            default => $this->letterPressedEvent($letter)
        };

        GameEvent::new(
            gameId: $this->gameId,
            event:  $eventData);

        $this->viewDataFromGameModel(Game::find($this->gameId));
    }

    /**
     * Sets the view data on this object for the given game.
     *
     * @param Game $game
     *
     * @return void
     */
    protected function viewDataFromGameModel(Game $game): void
    {
        $gameState = new CurrentState($game);

        $this->gameId = $gameState->gameModel->id;
        $this->secretWordLength = $gameState->gameModel->settings['wordLength'];
        $this->guesses = $gameState->guessedWords;
        $this->currentWordLetters = $gameState->currentWordLetters;
        $this->currentWordLength = count($this->currentWordLetters);
        $this->keys = $gameState->keyboardState->keys;
        $this->gameOver = $gameState->gameOver;
    }

    protected function enterPressedEvent(): data\GameEvent
    {
        return $this->currentWordLength === $this->secretWordLength
            ? new data\GameEvent(EventType::GuessSubmitted, [])
            : new data\GameEvent(EventType::InvalidKeypress, ['reason' => 'current word has insufficient letters']);
    }

    protected function deletePressedEvent(): data\GameEvent
    {
        return $this->currentWordLength > 0
            ? new data\GameEvent(EventType::LetterRemoved, [])
            : new data\GameEvent(EventType::InvalidKeypress, ['reason' => 'current word has no letters to be deleted']);
    }

    protected function letterPressedEvent(string $letter): data\GameEvent
    {
        return $this->currentWordLength < $this->secretWordLength
            ? new data\GameEvent(EventType::LetterAdded, ['letter' => $letter])
            : new data\GameEvent(EventType::InvalidKeypress, ['reason' => 'current word already has the necessary number of letters']);
    }
}
