<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\View\View;

class WelcomeController extends Controller
{
    public function index(): View
    {
        $mainMenu = [];

        if (Route::has('login')) {
            if (Auth::hasUser()) {
                $mainMenu[] = [
                    'href'  => route('dashboard'),
                    'title' => 'Dashboard',
                ];
            } else {
                $mainMenu = [
                    [
                        'href'  => route('login'),
                        'title' => 'Login',
                    ],
                    [
                        'href'  => \route('register'),
                        'title' => 'Register',
                    ],
                ];
            }
        }

        return view('welcome', compact(['mainMenu']));
    }
}
