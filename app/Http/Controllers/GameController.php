<?php

namespace App\Http\Controllers;

use App\Data\EventType;
use App\Game\CurrentState;
use App\Game\Settings as GameSettings;
use App\Models\Game;
use App\Models\GameEvent;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\View\View;

class GameController extends Controller
{
    public function new(): View
    {
        $settings = new GameSettings();

        return view('game.new', compact(['settings']));
    }

    public function store(Request $request): RedirectResponse
    {
        $wordLength = $request->input('wordLength');
        $settings = new GameSettings(
            word:       randomWord(wordLength: $wordLength),
            wordLength: $wordLength
        );

        $game = Game::create([
                                 'user_id'  => Auth::id(),
                                 'settings' => $settings,
                                 'slug'     => Str::uuid(),
                             ]);
        GameEvent::new(
            gameId: $game->id,
            event:  new \App\Data\GameEvent(
                        type: EventType::GameCreated,
                        data: ['settings' => $settings]),
        );

        return redirect()->route('game.show', ['game' => $game->slug]);
    }

    public function show(Game $game): View
    {
        return view('game.play', compact(['game']));
    }
}
