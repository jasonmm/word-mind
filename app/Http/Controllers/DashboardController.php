<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\View\View;

class DashboardController extends Controller
{
    public function index(): View
    {
        $mainMenu = [
            [
                'href'  => route('game.new'),
                'title' => 'New Game',
            ],
        ];

        return view('dashboard',
                    compact(['mainMenu']));
    }
}
