<?php

declare(strict_types=1);

namespace App\Game;

use App\Data\EventType;
use App\Data\KeyState;
use App\Models\Game;
use App\Models\GameEvent;
use Illuminate\Support\Collection;

/**
 * Contains the current game state.
 */
class CurrentState
{
    public function __construct(
        public Game           $gameModel,
        public ?array         $guessedWords = null,
        public array          $currentWordLetters = [],
        public ?KeyboardState $keyboardState = null,
        public bool           $gameOver = false,
    ) {
        if ($this->guessedWords === null) {
            $this->guessedWords = $this->guessedWords($this->gameModel->events);
        }
        $this->currentWordLetters = $this->currentWordLetters($this->gameModel->events);
        $this->gameOver = $this->gameOver($this->guessedWords);
        $this->keyboardState = $this->keyboardState($this->guessedWords);
    }

    /**
     * @param Collection<GameEvent> $events
     *
     * @return array
     */
    private function currentWordLetters(Collection $events): array
    {
        $word = new Collection();

        foreach ($events as $event) {
            if ($event->eventType() === EventType::LetterAdded) {
                $word->push([
                                'letter' => $event->event['data']['letter'],
                                'state'  => KeyState::NotInWord,
                            ]);
            } elseif ($event->eventType() === EventType::LetterRemoved) {
                $word->pop();
            } elseif ($event->eventType() === EventType::GuessSubmitted) {
                $word = new Collection();
            }
        }

        return $word->all();
    }

    private function letter(array $letterData)
    {
        return $letterData['letter'];
    }

    private function word(array $wordData): string
    {
        return collect($wordData)->map($this->letter(...))
                                 ->join('');
    }

    private function keyboardState(array $guessedWords): KeyboardState
    {
        $guessedLetters = collect($guessedWords)->flatMap($this->word(...))
                                                ->unique()
                                                ->join('');

        $keyState = function ($state, $letter) use ($guessedLetters) {
            return str_contains($guessedLetters, strtoupper($letter))
                ? KeyState::Used
                : KeyState::Unused;
        };

        $keyboardState = new KeyboardState();
        $keyboardState->keys = (collect($keyboardState->keys))->map($keyState)
                                                              ->all();

        return $keyboardState;
    }

    private function gameOver(array $lastGuess): bool
    {
        $lastGuess = collect($lastGuess)->map($this->word(...))
                                        ->last();

        return $lastGuess === $this->gameModel->settings['word'];
    }

    /**
     * @param Collection<GameEvent> $events
     *
     * @return array
     */
    private function guessedWords(Collection $events): array
    {
        $temp = new Collection();
        $words = new Collection();

        foreach ($events as $event) {
            if ($event->eventType() === EventType::LetterAdded) {
                $letter = $event->event['data']['letter'];
                $temp->push([
                                'letter' => $letter,
                                'state'  => $this->keyStateForGuessedLetter($letter, $temp->count()),
                            ]);
            } elseif ($event->eventType() === EventType::LetterRemoved) {
                $temp->pop();
            } elseif ($event->eventType() === EventType::GuessSubmitted) {
                $words->push($temp->all());
                $temp = new Collection();
            }
        }

        return $words->all();
    }

    private function keyStateForGuessedLetter(
        string $guessedLetter,
        int    $guessedLetterIndex): KeyState
    {
        $secretWord = $this->gameModel->settings['word'];
        $keyState = KeyState::NotInWord;

        foreach (str_split($secretWord) as $secretIndex => $secretLetter) {
            if ($secretIndex === $guessedLetterIndex && $secretLetter === $guessedLetter) {
                $keyState = KeyState::RightPosition;
            } elseif ($secretIndex !== $guessedLetterIndex && $secretLetter === $guessedLetter) {
                if ($keyState === KeyState::RightPosition) {
                    $keyState = KeyState::RightAndWrongPosition;
                } else {
                    $keyState = KeyState::WrongPosition;
                }
            }
        }

        return $keyState;
    }


}
