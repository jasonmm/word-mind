<?php

declare(strict_types=1);

namespace App\Game;

use App\Data\KeyState;

/**
 * Contains the state of the keys of the keyboard.
 */
class KeyboardState
{
    public array $keys = [
        'a' => KeyState::Unused,
        'b' => KeyState::Unused,
        'c' => KeyState::Unused,
        'd' => KeyState::Unused,
        'e' => KeyState::Unused,
        'f' => KeyState::Unused,
        'g' => KeyState::Unused,
        'h' => KeyState::Unused,
        'i' => KeyState::Unused,
        'j' => KeyState::Unused,
        'k' => KeyState::Unused,
        'l' => KeyState::Unused,
        'm' => KeyState::Unused,
        'n' => KeyState::Unused,
        'o' => KeyState::Unused,
        'p' => KeyState::Unused,
        'q' => KeyState::Unused,
        'r' => KeyState::Unused,
        's' => KeyState::Unused,
        't' => KeyState::Unused,
        'u' => KeyState::Unused,
        'v' => KeyState::Unused,
        'w' => KeyState::Unused,
        'x' => KeyState::Unused,
        'y' => KeyState::Unused,
        'z' => KeyState::Unused,
    ];
}
