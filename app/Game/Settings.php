<?php

declare(strict_types=1);

namespace App\Game;

use Illuminate\Support\Arr;
use JetBrains\PhpStorm\Pure;

/**
 * Game setting data.
 */
class Settings
{
    public function __construct(
        public string $word = '',
        public int    $wordLength = 5,
    ) {
    }

    #[Pure]
    public static function fromArray(array $settings): Settings
    {
        return new self(...Arr::only($settings, array_keys(get_class_vars(self::class))));
    }
}
