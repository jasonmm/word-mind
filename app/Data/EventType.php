<?php

declare(strict_types=1);

namespace App\Data;

enum EventType: string
{
    case GameCreated = 'GameCreated';
    case LetterAdded = 'LetterAdded';
    case LetterRemoved = 'LetterRemoved';
    case KeyPressed = 'KeyPressed';
    case GuessSubmitted = 'GuessSubmitted';
    case InvalidKeypress = 'InvalidKeypress';
}
