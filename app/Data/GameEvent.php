<?php

declare(strict_types=1);

namespace App\Data;

class GameEvent implements \JsonSerializable
{
    public function __construct(
        public EventType $type,
        public array     $data,
    ) {
    }

    public function jsonSerialize(): array
    {
        return get_object_vars($this);
    }
}
