<?php

declare(strict_types=1);

namespace App\Data;

enum KeyState: string
{
    case Unused = 'Unused';
    case Used = 'Used';
    case NotInWord = 'NotInWord';
    case WrongPosition = 'WrongPosition';
    case RightPosition = 'RightPosition';
    case RightAndWrongPosition = 'RightAndWrongPosition';
}
