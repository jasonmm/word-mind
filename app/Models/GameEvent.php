<?php

namespace App\Models;

use App\Data\EventType;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\GameEvent
 *
 * @property int                             $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int                             $game_id
 * @property array                           $event
 * @method static \Illuminate\Database\Eloquent\Builder|GameEvent
 *         newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|GameEvent newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|GameEvent query()
 * @method static \Illuminate\Database\Eloquent\Builder|GameEvent
 *         whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GameEvent
 *         whereEvent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GameEvent
 *         whereGameId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GameEvent
 *         whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GameEvent
 *         whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class GameEvent extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $casts = ['event' => 'array'];

    public static function new(int $gameId, \App\Data\GameEvent $event): GameEvent
    {
        return GameEvent::create(
            [
                'game_id' => $gameId,
                'event'   => $event,
            ]);
    }

    public function eventType(): ?EventType
    {
        return EventType::tryFrom($this->event['type']);
    }
}
