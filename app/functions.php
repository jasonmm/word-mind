<?php

declare(strict_types=1);


/**
 * @param string[] $wordList
 * @param int      $wordLength
 *
 * @return string
 */
function randomWord(array $wordList = [], int $wordLength = 0): string
{
    if (empty($wordList)) {
        $wordList = array_map(
            fn($l) => trim($l),
            file(storage_path('app/words.txt'))
        );
    }

    $wordList = collect($wordList);
    if ($wordLength > 3) {
        $wordList = $wordList->filter(fn($w) => strlen($w) === $wordLength);
    }

    return $wordList->random();
}
