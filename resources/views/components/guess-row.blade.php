<table class="mx-auto my-2">
    <tbody>
    <tr>
        @foreach(array_pad($word, $wordLength, ['letter'=>'', 'state'=>\App\Data\KeyState::NotInWord]) as $letter)
            <td class="px-1">
                <x-guess-letter :state="$letter['state']">
                    {{ $letter['letter'] }}
                </x-guess-letter>
            </td>
        @endforeach
    </tr>
    </tbody>
</table>
