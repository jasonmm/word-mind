<div class="max-w-sm mx-auto md:max-w-lg">
    <div class="w-full">
        <ul class="px-0">
            @foreach($items as $item)
                <li class="border bg-inherit list-none rounded-lg cursor-pointer
                           text-center my-1 hover:font-bold hover:text-black
                           hover:bg-sky-50 dark:text-white">
                    <a href="{{ $item['href'] }}"
                       class="w-full h-full block py-3 px-3">
                        {{ $item['title'] }}
                    </a>
                </li>
            @endforeach
        </ul>
    </div>
</div>
