<div wire:click="keypress($event.target.innerText)"
     class="text-center w-fit border dark:border-gray-400 cursor-pointer rounded p-4 text-black dark:text-neutral-300
     @switch($state)
        @case(\App\Data\KeyState::Used):
            bg-amber-500 dark:bg-amber-900
        @default:
            bg-sky-100 dark:bg-slate-800
     @endswitch"
     xmlns:wire="http://www.w3.org/1999/xhtml">
    {{ $slot }}
</div>
