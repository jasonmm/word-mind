<div class="font-mono h-2/6 min-h-full my-3 items-center">
    <x-keyboard-row :keys="['Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P']"
                    :keysStates="$keysStates"></x-keyboard-row>
    <x-keyboard-row :keys="['A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L']"
                    :keysStates="$keysStates"></x-keyboard-row>
    <x-keyboard-row :keys="['ENT', 'Z', 'X', 'C', 'V', 'B', 'N', 'M', 'DEL']"
                    :keysStates="$keysStates"></x-keyboard-row>
</div>
