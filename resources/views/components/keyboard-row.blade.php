<div>
    <table class="mx-auto">
        <tbody>
        <tr>
            @foreach($keys as $key)
                <td>
                    <x-keyboard-key :state="$keysStates[strtolower($key)] ?? null">{{ $key }}</x-keyboard-key>
                </td>
            @endforeach
        </tr>
        </tbody>
    </table>
</div>
