<div
    class="text-center text-xl w-fit border rounded p-2 dark:border-gray-400
    @switch($state)
         @case(\App\Data\KeyState::RightPosition)
            bg-green-500 dark:bg-green-800 text-black dark:text-stone-300
            @break
         @case(\App\Data\KeyState::WrongPosition)
            bg-yellow-300 dark:bg-yellow-600 text-black dark:text-black-300
            @break
        @case(\App\Data\KeyState::RightAndWrongPosition)
            bg-violet-400 dark:bg-violet-800 text-black dark:text-neutral-300
            @break
        @default
            bg-sky-200 dark:bg-sky-700 text-black dark:text-neutral-300
    @endswitch">
    {{ $slot }}
</div>
