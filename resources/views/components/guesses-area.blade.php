<div class="font-mono mx-auto px-3 w-max h-3/6 overflow-y-auto">
    @foreach($guesses as $guess)
        <x-guess-row :word="$guess" :wordLength="$wordLength"></x-guess-row>
    @endforeach

    @if($gameOver === false )
        <x-guess-row :word="$currentWord"
                     :wordLength="$wordLength"></x-guess-row>
    @endif
</div>
