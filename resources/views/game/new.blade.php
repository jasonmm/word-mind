<x-app-layout>
    <div class="container p-12">
        <div class="font-bold text-2xl text-black dark:text-white mb-4">
            New Game Settings
        </div>

        <form action="{{ route('game.store') }}" method="post">
            @csrf

            <div>
                <x-jet-label for="word-length">Word Length</x-jet-label>
                <x-jet-input type="number" id="word-length"
                             name="wordLength"
                             size="3" max="7" min="4"
                             value="{{ $settings->wordLength }}"></x-jet-input>
            </div>

            <div class="mt-5">
                <x-jet-button>Start Game</x-jet-button>
            </div>
        </form>
    </div>
</x-app-layout>
