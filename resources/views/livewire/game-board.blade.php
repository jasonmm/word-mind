<div class="w-full">
    <div class="h-full overflow-y-hidden">
        <x-guesses-area :guesses="$guesses"
                        :currentWord="$currentWordLetters"
                        :wordLength="$secretWordLength"
                        :gameOver="$gameOver"></x-guesses-area>

        @if($gameOver === false)
            <x-keyboard :keysStates="$keys"></x-keyboard>
        @else
            <div class="mx-auto my-5 text-center">
                <strong class="text-2xl">Game Over!</strong>
            </div>
        @endif
    </div>
</div>

