<div class="flex justify-center">
    <div class="font-bold text-3xl items-center dark:text-white">
        <a href="{{ route('welcome') }}">
            {{ config('app.name') }}
        </a>
    </div>
</div>
<hr>
